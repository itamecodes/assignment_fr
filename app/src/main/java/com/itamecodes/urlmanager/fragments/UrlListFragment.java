package com.itamecodes.urlmanager.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.itamecodes.urlmanager.R;
import com.itamecodes.urlmanager.adapters.UrlCursorAdapter;
import com.itamecodes.urlmanager.dialogs.AddBookmarkDialog;
import com.itamecodes.urlmanager.provider.url.UrlColumns;
import com.itamecodes.urlmanager.provider.url.UrlSelection;
import com.itamecodes.urlmanager.utils.DividerItemDecoration;

/**
 * Created by vivek on 3/1/16.
 */
public class UrlListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, UrlCursorAdapter.OnItemClickListener {
    private static final String SEARCH_STRING = "searchstring";
    private static final String TAG_DIALOG = "dialog_add_bookmark";
    FloatingActionButton mAddBookmarkButton;
    private UrlCursorAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private TextView mNoContent;

    private Button mSearchButton;
    private EditText mSearch_et;
    private String mSearchText;
    private Button mClearSearchButton;

    public UrlListFragment() {

    }

    public static UrlListFragment getInstance() {
        UrlListFragment frag = new UrlListFragment();
        return frag;
    }

    @Override
    public void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        this.mAdapter = new UrlCursorAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_urllist_layout, container, false);
        mAddBookmarkButton = (FloatingActionButton) view.findViewById(R.id.fab_addbookmark);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.urllistview);
        mSearch_et = (EditText) view.findViewById(R.id.search_et);
        mSearchButton = (Button) view.findViewById(R.id.button_search);
        mClearSearchButton = (Button) view.findViewById(R.id.button_clear);
        mNoContent = (TextView) view.findViewById(R.id.no_content);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(
                getActivity().getApplicationContext()
        ));
        mAdapter.setOnItemClickListener(this);

        if (savedInstanceState != null) {
            mSearchText = savedInstanceState.getString(SEARCH_STRING);
        }

        if (mSearchText == null || mSearchText.isEmpty()) {
            startLoaderForAllData();
        } else {
            startLoaderForFilteredData(mSearchText);
        }

        //set the item touch helper .. swipe to delete
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(final RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder, final RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, final int swipeDir) {
                Cursor c = mAdapter.getItem(viewHolder.getAdapterPosition());
                String urltoDelete = c.getString(c.getColumnIndex(UrlColumns.URL));
                UrlSelection where = new UrlSelection();
                where.url(urltoDelete);
                where.delete(getActivity().getContentResolver());

            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchBookmark();
            }
        });
        mClearSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSearch();
            }
        });

        itemTouchHelper.attachToRecyclerView(mRecyclerView);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAddBookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddBookmarkDialog dialogFragment = AddBookmarkDialog.newInstance();
                dialogFragment.show(getFragmentManager(), TAG_DIALOG);
            }
        });

        return view;
    }

    private void searchBookmark() {
        mSearchText = mSearch_et.getText().toString();
        if (mSearchText != null && !mSearchText.isEmpty()) {
            startLoaderForFilteredData(mSearchText);
        } else {
            Snackbar.make(mRecyclerView, R.string.empty_search, Snackbar.LENGTH_LONG).show();
        }
    }

    private void resetSearch() {
        mSearchText = null;
        mSearch_et.setText(null);
        startLoaderForAllData();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        Uri CONTENT_URI = new UrlSelection().uri();
        switch (loaderId) {
            case 1:
                return new CursorLoader(getActivity(), CONTENT_URI, null, null, null, null);
            case 2:
                String searchText = args.getString(SEARCH_STRING);
                String selection = UrlColumns.TITLE + " LIKE ? or " + UrlColumns.URL + " LIKE ? COLLATE NOCASE";
                return new CursorLoader(getActivity(), CONTENT_URI, null, selection, new String[]{"%" + searchText + "%", "%" + searchText + "%"}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case 1: {
                mNoContent.setText(getString(R.string.no_content));
                swapRecyclerViewWithEmptyDataContainer(cursor);
                break;
            }
            case 2: {
                mNoContent.setText(getString(R.string.no_search_result));
                swapRecyclerViewWithEmptyDataContainer(cursor);
                break;
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.mAdapter.swapCursor(null);
    }

    @Override
    public void onItemClicked(Cursor cursor) {
        String url = cursor.getString(cursor.getColumnIndex(UrlColumns.URL));
        if (url != null && !url.isEmpty()) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } else {
            Snackbar.make(mRecyclerView, R.string.wrong_url, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SEARCH_STRING, mSearch_et.getText().toString());
    }

    public void startLoaderForAllData() {
        getLoaderManager().restartLoader(1, null, this);
    }

    public void startLoaderForFilteredData(String searchText) {
        Bundle bun = new Bundle();
        bun.putString(SEARCH_STRING, searchText);
        getLoaderManager().restartLoader(2, bun, this);
    }

    public void swapRecyclerViewWithEmptyDataContainer(Cursor cursor) {
        if (cursor.getCount() == 0) {
            mNoContent.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mNoContent.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            this.mAdapter.swapCursor(cursor);
        }
    }
}
