package com.itamecodes.urlmanager.provider.url;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.itamecodes.urlmanager.provider.base.AbstractSelection;

/**
 * Selection for the {@code url} table.
 */
public class UrlSelection extends AbstractSelection<UrlSelection> {
    @Override
    protected Uri baseUri() {
        return UrlColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code UrlCursor} object, which is positioned before the first entry, or null.
     */
    public UrlCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new UrlCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public UrlCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code UrlCursor} object, which is positioned before the first entry, or null.
     */
    public UrlCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new UrlCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public UrlCursor query(Context context) {
        return query(context, null);
    }


    public UrlSelection id(long... value) {
        addEquals("url." + UrlColumns._ID, toObjectArray(value));
        return this;
    }

    public UrlSelection idNot(long... value) {
        addNotEquals("url." + UrlColumns._ID, toObjectArray(value));
        return this;
    }

    public UrlSelection orderById(boolean desc) {
        orderBy("url." + UrlColumns._ID, desc);
        return this;
    }

    public UrlSelection orderById() {
        return orderById(false);
    }

    public UrlSelection title(String... value) {
        addEquals(UrlColumns.TITLE, value);
        return this;
    }

    public UrlSelection titleNot(String... value) {
        addNotEquals(UrlColumns.TITLE, value);
        return this;
    }

    public UrlSelection titleLike(String... value) {
        addLike(UrlColumns.TITLE, value);
        return this;
    }

    public UrlSelection titleContains(String... value) {
        addContains(UrlColumns.TITLE, value);
        return this;
    }

    public UrlSelection titleStartsWith(String... value) {
        addStartsWith(UrlColumns.TITLE, value);
        return this;
    }

    public UrlSelection titleEndsWith(String... value) {
        addEndsWith(UrlColumns.TITLE, value);
        return this;
    }

    public UrlSelection orderByTitle(boolean desc) {
        orderBy(UrlColumns.TITLE, desc);
        return this;
    }

    public UrlSelection orderByTitle() {
        orderBy(UrlColumns.TITLE, false);
        return this;
    }

    public UrlSelection url(String... value) {
        addEquals(UrlColumns.URL, value);
        return this;
    }

    public UrlSelection urlNot(String... value) {
        addNotEquals(UrlColumns.URL, value);
        return this;
    }

    public UrlSelection urlLike(String... value) {
        addLike(UrlColumns.URL, value);
        return this;
    }

    public UrlSelection urlContains(String... value) {
        addContains(UrlColumns.URL, value);
        return this;
    }

    public UrlSelection urlStartsWith(String... value) {
        addStartsWith(UrlColumns.URL, value);
        return this;
    }

    public UrlSelection urlEndsWith(String... value) {
        addEndsWith(UrlColumns.URL, value);
        return this;
    }

    public UrlSelection orderByUrl(boolean desc) {
        orderBy(UrlColumns.URL, desc);
        return this;
    }

    public UrlSelection orderByUrl() {
        orderBy(UrlColumns.URL, false);
        return this;
    }

    public UrlSelection createdAt(String... value) {
        addEquals(UrlColumns.CREATED_AT, value);
        return this;
    }

    public UrlSelection createdAtNot(String... value) {
        addNotEquals(UrlColumns.CREATED_AT, value);
        return this;
    }

    public UrlSelection createdAtLike(String... value) {
        addLike(UrlColumns.CREATED_AT, value);
        return this;
    }

    public UrlSelection createdAtContains(String... value) {
        addContains(UrlColumns.CREATED_AT, value);
        return this;
    }

    public UrlSelection createdAtStartsWith(String... value) {
        addStartsWith(UrlColumns.CREATED_AT, value);
        return this;
    }

    public UrlSelection createdAtEndsWith(String... value) {
        addEndsWith(UrlColumns.CREATED_AT, value);
        return this;
    }

    public UrlSelection orderByCreatedAt(boolean desc) {
        orderBy(UrlColumns.CREATED_AT, desc);
        return this;
    }

    public UrlSelection orderByCreatedAt() {
        orderBy(UrlColumns.CREATED_AT, false);
        return this;
    }

    public UrlSelection updatedAt(String... value) {
        addEquals(UrlColumns.UPDATED_AT, value);
        return this;
    }

    public UrlSelection updatedAtNot(String... value) {
        addNotEquals(UrlColumns.UPDATED_AT, value);
        return this;
    }

    public UrlSelection updatedAtLike(String... value) {
        addLike(UrlColumns.UPDATED_AT, value);
        return this;
    }

    public UrlSelection updatedAtContains(String... value) {
        addContains(UrlColumns.UPDATED_AT, value);
        return this;
    }

    public UrlSelection updatedAtStartsWith(String... value) {
        addStartsWith(UrlColumns.UPDATED_AT, value);
        return this;
    }

    public UrlSelection updatedAtEndsWith(String... value) {
        addEndsWith(UrlColumns.UPDATED_AT, value);
        return this;
    }

    public UrlSelection orderByUpdatedAt(boolean desc) {
        orderBy(UrlColumns.UPDATED_AT, desc);
        return this;
    }

    public UrlSelection orderByUpdatedAt() {
        orderBy(UrlColumns.UPDATED_AT, false);
        return this;
    }
}
