package com.itamecodes.urlmanager.provider.url;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itamecodes.urlmanager.provider.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code url} table.
 */
public class UrlCursor extends AbstractCursor implements UrlModel {
    public UrlCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(UrlColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Name of bookmark
     * Cannot be {@code null}.
     */
    @NonNull
    public String getTitle() {
        String res = getStringOrNull(UrlColumns.TITLE);
        if (res == null)
            throw new NullPointerException("The value of 'title' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Url of bookmark
     * Cannot be {@code null}.
     */
    @NonNull
    public String getUrl() {
        String res = getStringOrNull(UrlColumns.URL);
        if (res == null)
            throw new NullPointerException("The value of 'url' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code created_at} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getCreatedAt() {
        String res = getStringOrNull(UrlColumns.CREATED_AT);
        if (res == null)
            throw new NullPointerException("The value of 'created_at' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code updated_at} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getUpdatedAt() {
        String res = getStringOrNull(UrlColumns.UPDATED_AT);
        if (res == null)
            throw new NullPointerException("The value of 'updated_at' in the database was null, which is not allowed according to the model definition");
        return res;
    }
}
