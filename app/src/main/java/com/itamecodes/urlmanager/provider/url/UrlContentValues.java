package com.itamecodes.urlmanager.provider.url;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.itamecodes.urlmanager.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code url} table.
 */
public class UrlContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return UrlColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable UrlSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable UrlSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Name of bookmark
     */
    public UrlContentValues putTitle(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("title must not be null");
        mContentValues.put(UrlColumns.TITLE, value);
        return this;
    }


    /**
     * Url of bookmark
     */
    public UrlContentValues putUrl(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("url must not be null");
        mContentValues.put(UrlColumns.URL, value);
        return this;
    }


    public UrlContentValues putCreatedAt(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("createdAt must not be null");
        mContentValues.put(UrlColumns.CREATED_AT, value);
        return this;
    }


    public UrlContentValues putUpdatedAt(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("updatedAt must not be null");
        mContentValues.put(UrlColumns.UPDATED_AT, value);
        return this;
    }

}
