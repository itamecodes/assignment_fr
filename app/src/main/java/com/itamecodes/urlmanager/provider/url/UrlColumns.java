package com.itamecodes.urlmanager.provider.url;

import android.net.Uri;
import android.provider.BaseColumns;

import com.itamecodes.urlmanager.provider.URLManagerContentProvider;
import com.itamecodes.urlmanager.provider.url.UrlColumns;

/**
 * Table for managing urls
 */
public class UrlColumns implements BaseColumns {
    public static final String TABLE_NAME = "url";
    public static final Uri CONTENT_URI = Uri.parse(URLManagerContentProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    /**
     * Name of bookmark
     */
    public static final String TITLE = "title";

    /**
     * Url of bookmark
     */
    public static final String URL = "url";

    public static final String CREATED_AT = "created_at";

    public static final String UPDATED_AT = "updated_at";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            TITLE,
            URL,
            CREATED_AT,
            UPDATED_AT
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(TITLE) || c.contains("." + TITLE)) return true;
            if (c.equals(URL) || c.contains("." + URL)) return true;
            if (c.equals(CREATED_AT) || c.contains("." + CREATED_AT)) return true;
            if (c.equals(UPDATED_AT) || c.contains("." + UPDATED_AT)) return true;
        }
        return false;
    }

}
