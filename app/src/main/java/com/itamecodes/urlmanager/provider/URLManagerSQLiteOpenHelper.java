package com.itamecodes.urlmanager.provider;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.itamecodes.urlmanager.BuildConfig;
import com.itamecodes.urlmanager.provider.url.UrlColumns;

public class URLManagerSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = URLManagerSQLiteOpenHelper.class.getSimpleName();

    public static final String DATABASE_FILE_NAME = "urlmanager.db";
    private static final int DATABASE_VERSION = 1;
    private static URLManagerSQLiteOpenHelper sInstance;
    private final Context mContext;
    private final URLManagerSQLiteOpenHelperCallbacks mOpenHelperCallbacks;

    // @formatter:off
    public static final String SQL_CREATE_TABLE_URL = "CREATE TABLE IF NOT EXISTS "
            + UrlColumns.TABLE_NAME + " ( "
            + UrlColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UrlColumns.TITLE + " TEXT NOT NULL, "
            + UrlColumns.URL + " TEXT NOT NULL, "
            + UrlColumns.CREATED_AT + " TEXT NOT NULL, "
            + UrlColumns.UPDATED_AT + " TEXT NOT NULL "
            + ", CONSTRAINT unique_url UNIQUE (url) ON CONFLICT REPLACE"
            + " );";

    // @formatter:on

    public static URLManagerSQLiteOpenHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = newInstance(context.getApplicationContext());
        }
        return sInstance;
    }

    private static URLManagerSQLiteOpenHelper newInstance(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return newInstancePreHoneycomb(context);
        }
        return newInstancePostHoneycomb(context);
    }


    /*
     * Pre Honeycomb.
     */
    private static URLManagerSQLiteOpenHelper newInstancePreHoneycomb(Context context) {
        return new URLManagerSQLiteOpenHelper(context);
    }

    private URLManagerSQLiteOpenHelper(Context context) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
        mContext = context;
        mOpenHelperCallbacks = new URLManagerSQLiteOpenHelperCallbacks();
    }


    /*
     * Post Honeycomb.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static URLManagerSQLiteOpenHelper newInstancePostHoneycomb(Context context) {
        return new URLManagerSQLiteOpenHelper(context, new DefaultDatabaseErrorHandler());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private URLManagerSQLiteOpenHelper(Context context, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION, errorHandler);
        mContext = context;
        mOpenHelperCallbacks = new URLManagerSQLiteOpenHelperCallbacks();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        mOpenHelperCallbacks.onPreCreate(mContext, db);
        db.execSQL(SQL_CREATE_TABLE_URL);
        mOpenHelperCallbacks.onPostCreate(mContext, db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            setForeignKeyConstraintsEnabled(db);
        }
        mOpenHelperCallbacks.onOpen(mContext, db);
    }

    private void setForeignKeyConstraintsEnabled(SQLiteDatabase db) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setForeignKeyConstraintsEnabledPreJellyBean(db);
        } else {
            setForeignKeyConstraintsEnabledPostJellyBean(db);
        }
    }

    private void setForeignKeyConstraintsEnabledPreJellyBean(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setForeignKeyConstraintsEnabledPostJellyBean(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mOpenHelperCallbacks.onUpgrade(mContext, db, oldVersion, newVersion);
    }
}
