package com.itamecodes.urlmanager.provider.url;

import com.itamecodes.urlmanager.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Table for managing urls
 */
public interface UrlModel extends BaseModel {

    /**
     * Name of bookmark
     * Cannot be {@code null}.
     */
    @NonNull
    String getTitle();

    /**
     * Url of bookmark
     * Cannot be {@code null}.
     */
    @NonNull
    String getUrl();

    /**
     * Get the {@code created_at} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getCreatedAt();

    /**
     * Get the {@code updated_at} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getUpdatedAt();
}
