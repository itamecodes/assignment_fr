package com.itamecodes.urlmanager.dialogs;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.DialogFragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.itamecodes.urlmanager.R;
import com.itamecodes.urlmanager.provider.url.UrlContentValues;

import java.util.Calendar;

/**
 * Created by vivek on 3/1/16.
 */
public class AddBookmarkDialog extends DialogFragment {
    private EditText mBookmarkName_et;
    private EditText mBookmarkUrl_et;
    private PercentRelativeLayout mContainer;
    private Button mSave;


    public AddBookmarkDialog() {

    }

    public static AddBookmarkDialog newInstance() {
        AddBookmarkDialog addBookmarkDialog = new AddBookmarkDialog();
        return addBookmarkDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.addbookmarkdialog_layout, container);
        getDialog().setTitle(getString(R.string.add_bookmark_dialog_title));
        mContainer = (PercentRelativeLayout) view.findViewById(R.id.addbookmark_container);
        mBookmarkName_et = (EditText) view.findViewById(R.id.bookmarkname_et);
        mBookmarkUrl_et = (EditText) view.findViewById(R.id.bookmarkurl_et);
        mSave = (Button) view.findViewById(R.id.savebookmark);
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveBookmark();
            }
        });
        setCancelable(true);
        return view;
    }


    public void saveBookmark() {
        String bookmarkName = mBookmarkName_et.getText().toString();
        String bookmarkUrl = mBookmarkUrl_et.getText().toString();
        boolean isValid = true;
        if (bookmarkName == null || bookmarkName.isEmpty()) {
            mBookmarkName_et.setError(getString(R.string.error_empty));
            isValid = false;
        }
        if (bookmarkUrl == null || bookmarkUrl.isEmpty()) {
            mBookmarkUrl_et.setError(getString(R.string.error_empty));
            isValid = false;
        } else {
            if (!Patterns.WEB_URL.matcher(bookmarkUrl).matches()) {
                mBookmarkUrl_et.setError(getString(R.string.error_bad_url));
                isValid = false;
            }
        }
        if (isValid) {
            Calendar cal = Calendar.getInstance();
            String presentTimeStamp = String.valueOf(cal.getTimeInMillis());
            UrlContentValues values = new UrlContentValues();
            values.putTitle(bookmarkName).putUrl(bookmarkUrl).putCreatedAt(presentTimeStamp).putUpdatedAt(presentTimeStamp);
            Uri inserturi = values.insert(getActivity().getContentResolver());
            getDialog().cancel();
        } else {
            Snackbar.make(mContainer, R.string.error_add_bookmark, Snackbar.LENGTH_LONG).show();
        }
    }

}
