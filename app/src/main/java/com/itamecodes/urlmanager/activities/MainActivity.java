package com.itamecodes.urlmanager.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.itamecodes.urlmanager.R;
import com.itamecodes.urlmanager.fragments.UrlListFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "URL_LIST_FRAGMENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        UrlListFragment frag = (UrlListFragment) getSupportFragmentManager().findFragmentByTag(TAG);

        if (frag == null) {
            frag = UrlListFragment.getInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.urllist_layout_container, frag, TAG).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // dont have any menu items .. yet keep it
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
