package com.itamecodes.urlmanager.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itamecodes.urlmanager.R;
import com.itamecodes.urlmanager.provider.url.UrlColumns;

/**
 * Created by vivek on 3/1/16.
 */
public class UrlCursorAdapter extends RecyclerViewCursorAdapter<UrlCursorAdapter.UrlResultViewHolder> implements View.OnClickListener {
    private final LayoutInflater layoutInflater;
    private OnItemClickListener onItemClickListener;
    private Context mContext;

    public UrlCursorAdapter(final Context ctx){
        super();
        this.layoutInflater=LayoutInflater.from(ctx);
        mContext=ctx;
    }

    public void setOnItemClickListener(final OnItemClickListener onItemClickListener)
    {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public UrlResultViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
    {
        final View view = this.layoutInflater.inflate(R.layout.urllist_row, parent, false);
        view.setOnClickListener(this);

        return new UrlResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UrlResultViewHolder holder, final int position) {
        super.onBindViewHolder(holder,position);
       if(position%2==0){
           holder.listItemContainer.setBackgroundColor(ContextCompat.getColor(mContext,R.color.border_gray));
       }else{
           holder.listItemContainer.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
       }
    }

    @Override
    public void onBindViewHolder(UrlResultViewHolder holder, Cursor cursor) {
        holder.bindData(cursor);
    }

    @Override
    public void onClick(View view) {
        if (this.onItemClickListener != null)
        {
            final RecyclerView recyclerView = (RecyclerView) view.getParent();
            final int position = recyclerView.getChildLayoutPosition(view);
            if (position != RecyclerView.NO_POSITION)
            {
                final Cursor cursor = this.getItem(position);
                this.onItemClickListener.onItemClicked(cursor);
            }
        }
    }

    public interface OnItemClickListener
    {
        void onItemClicked(Cursor cursor);
    }

    public static class UrlResultViewHolder extends RecyclerView.ViewHolder
    {
        public LinearLayout listItemContainer;
        private TextView mBookmarkNameTv;
        private TextView mBookmarkURLTv;

        public UrlResultViewHolder(final View itemView)
        {
            super(itemView);
            listItemContainer=(LinearLayout)itemView.findViewById(R.id.listitem_container);
            mBookmarkNameTv= (TextView) itemView.findViewById(R.id.bookmark_name);
            mBookmarkURLTv= (TextView) itemView.findViewById(R.id.bookmark_url);
        }

        public void bindData(final Cursor cursor)
        {
            final String name = cursor.getString(cursor.getColumnIndex(UrlColumns.TITLE));
            final String url = cursor.getString(cursor.getColumnIndex(UrlColumns.URL));
            this.mBookmarkNameTv.setText(name);
            this.mBookmarkURLTv.setText(url);
        }
    }



}
