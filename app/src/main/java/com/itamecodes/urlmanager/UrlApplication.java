package com.itamecodes.urlmanager;

import android.app.Application;

/**
 * Created by vivek on 3/1/16.
 */
public class UrlApplication extends Application {

    @Override
    public void onCreate() {
        //Just in case  something needs to be done with the application class
        super.onCreate();
    }
}
